<?php

	/**
	* Parent CLass
	*/
	class Car {
		
		private $model;

		public function setModel($model) {
			$this->model = $model;
		}

		public function getModel() {
			return $this->model;
		}
	}

	/**
	* Extended Class
	*/
	class Sports extends Car
	{
		private $style = 'Fast and Furious';

		public function drive() {
			return 'Drive a '.$this->getModel().' with '.$this->style;
		}
	}

	$load = new Sports();
	$load->setModel('BMW');
	echo $load->drive();

?>