<?php

	// Create Class
	class car {
		
		// Properties
		public $color = 'blue';
		private $brand = 'BMW';

		// Method
		public function hello() {
			return "I like ".$this->color." color cars.";		// $this is a keyword
		}

	}

	// Create an instance
	$new = new car();

	// Set the values
	$new->color = 'black';

	// Get the values
	echo $new->color;
	echo "<br>". $new->hello();

?>