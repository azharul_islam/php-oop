<?php

	/*----- Method 01 ----- */

	class car {
		private $model;

		public function __construct($model) {
			$this->model = $model;
		}

		public function getcar() {
			return 'This car model is: '.$this->model;
		}
	}

	/* ----- Method 02 ----- */

	class car {
		private $model = "Toyota";

		public function __construct($model = null) {
			if($model) {
				$this->model = $model;
			}
		}

		public function getcar() {
			return 'The '.__class__.' model is: '.$this->model;				//	__class__ is for called the class name
		}
	}

	$load = new car();
	echo $load->getcar();

?>