<?php

	/**
	*  Create Class
	*/
	class car {
		
		// Properties
		public $tank;

		// Methods
		public function fill($float) {
			$this->tank += $float;

			return $this;
		}

		public function ride($float) {
			$miles = $float;
			$gallons = $miles/50;
			$this->tank -= $gallons;

			return $this;
		}
	}

	$load = new car();
	$tank = $load->fill(10)->ride(20)->tank;

	echo "The number of gallons left in the tank: " . $tank . " gal.";

?>